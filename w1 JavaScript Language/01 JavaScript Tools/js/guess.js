let targetNumber = Math.floor(Math.random() * 10) + 1;
let guess = 0;
function init () {
    
    document.getElementById('form').addEventListener('submit', function(event){
        event.preventDefault();
        check(document.getElementById('num').value);
        });
    
	
}

function check (value) {
  var num = value;
  
    if (num == targetNumber){
      showWin();
      
  } 
  else if(num != targetNumber && guess < 5) {
      guess++;
      showError();
      
  }
  else if(guess >= 5){
      showLoss();
      guess = 0;
  }  
  
  
  
}

function showWin () {
  document.getElementById('result').innerHTML = "Voitto";
}

function showError () {
  document.getElementById('result').innerHTML = "Väärin";
}

function showLoss () {
  document.getElementById('result').innerHTML = "Häviö";
}

init();
